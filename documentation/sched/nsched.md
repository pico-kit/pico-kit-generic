
nbitarray - Neon Bit Array
==========================

The Neon Bit Array represents a data structure that holds 256 bits. Each bit
is represented by an address. Address space is linear and goes from 0 to 255.
Each bit can be set and reset. The msbs() (Most Significant Bit Set) function
will return the most significant set bit in the array.
