
#include <stdbool.h>

#include "common/pk_bits.h"
#include "common/pk_port.h"
#include "tasker/pk_completion.h"
#include "pico-kit_manifest.h"

#if !defined(NEON8__MANIFEST__COMPLETIONS)
#error "Invalid manifest: NEON8__MANIFEST__COMPLETIONS not defined."
#endif

#if (NEON8__MANIFEST__COMPLETIONS > 0)

enum completion_state
{
    COMPLETION__STATE__UNINITIALIZED,
    COMPLETION__STATE__ACTIVE,
    COMPLETION__STATE__TERMINATED
};

struct completion
{
    bool complete;
    enum completion_state state;
    pk_task__id task_id;
};

static struct completion g_completion__pool[NEON8__MANIFEST__COMPLETIONS];

pk_completion__id
pk_completion__create(pk_task__id task_id)
{
    pk_completion__id retval = PK_COMPLETION__NULL_ID;
    for (pk_completion__id id = 0u; id < PK_BITS__ARRAY_SIZE(g_completion__pool); id++) {
        if (g_completion__pool[id].state == COMPLETION__STATE__UNINITIALIZED) {
            g_completion__pool[id].state = COMPLETION__STATE__ACTIVE;
            g_completion__pool[id].complete = false;
            g_completion__pool[id].task_id = task_id;
            retval = id;
            break;
        }
    }
    return retval;
}

void
pk_completion__complete_isr(pk_completion__id id)
{
    g_completion__pool[id].complete = true;
    (void) pk_task__unblock(g_completion__pool[id].task_id);
}

bool
pk_completion__complete(pk_completion__id id)
{
    bool retval;
    pk_port__isr_state isr_state;

    g_completion__pool[id].complete = true;
    isr_state = pk_port__isr_disable();
    retval = pk_task__unblock(g_completion__pool[id].task_id);
    pk_port__isr_restore(isr_state);
    return retval;
}

bool
pk_completion__is_completed(pk_completion__id id)
{
    bool retval;

    if (g_completion__pool[id].complete) {
        g_completion__pool[id].complete = false;
        retval = false;
    } else {
        retval = true;
    }
    return retval;
}

void
pk_completion__reset(pk_completion__id id)
{
    g_completion__pool[id].complete = false;
}

#endif /* (NEON8__MANIFEST__COMPLETIONS > 0) */
