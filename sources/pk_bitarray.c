#include "composite/pk_bitarray.h"

#include <string.h>
#include "common/pk_bits.h"
#include "common/pk_port.h"

void
pk_bitarray__init(struct pk_bitarray * ba)
{
    memset(ba, 0, sizeof(*ba));
}

void
pk_bitarray__set(struct pk_bitarray * ba, uint_fast8_t bit)
{
    uint_fast8_t column_idx = bit >> 3;

    pk_port__set_bit(&ba->rows[column_idx], bit & 0x7);
    pk_port__set_bit(&ba->column, column_idx);
}

void
pk_bitarray__clear(struct pk_bitarray * ba, uint_fast8_t bit)
{
    uint_fast8_t column_idx = bit >> 3;

    pk_port__clear_bit(&ba->rows[column_idx], bit & 0x7);

    if (ba->rows[column_idx] == 0u) {
        pk_port__clear_bit(&ba->column, column_idx);
    }
}

uint_fast8_t
pk_bitarray__msbs(const struct pk_bitarray * ba)
{
    uint_fast8_t column_idx;

    column_idx = narch__log2(ba->column);

    return narch__log2(ba->rows[column_idx]) + (uint_fast8_t) (column_idx << 3);
}

uint_fast8_t
pk_bitarray__msbs_and_clear(struct pk_bitarray * ba)
{
    uint_fast8_t retval;

    retval = pk_bitarray__msbs(ba);
    pk_bitarray__clear(ba, retval);

    return retval;
}
