
#include "common/pk_port.h"
#include "common/pk_bits.h"
#include "composite/pk_bitarray.h"
#include "tasker/pk_task.h"
#include "pico-kit_manifest.h"

#include <stdbool.h>
#include <assert.h>

#define INVALID_HANDLE_PRIO     UINT_FAST8_MAX

struct task__tcb
{
    pk_task__context context;
    pk_task__state state;
    pk_task__fn * fn;
    void * arg;
};

struct task__rq
{
    struct pk_bitarray group;

};

static struct task__rq g_task__rq;
static struct task__tcb g_tasks[NEON8__MANIFEST__TASKS];

pk_task__id pk_task__current_id;

pk_task__id
pk_task__create(uint8_t priority, pk_task__fn * fn, void * arg)
{
    pk_task__id retval;
    assert(priority < PK_BITS__ARRAY_SIZE(g_tasks));
    if (g_tasks[priority].state == PK_TASK__STATE_DORMANT) {
        g_tasks[priority].state = PK_TASK__STATE_WAITING;
        g_tasks[priority].fn = fn;
        g_tasks[priority].arg = arg;
        g_tasks[priority].context = 0u;
        retval = priority;
    } else {
        retval = PK_TASK__NULL_ID;
    }
    return retval;
}

bool
pk_task__unblock(pk_task__id id)
{
    bool retval;
    pk_bitarray__set(&g_task__rq.group, id);
    retval = id < pk_task__current_id;
    return retval;
}

void
pk_task__schedule(void)
{

    while (true) {
        while (!pk_bitarray__is_empty(&g_task__rq.group)) {
            pk_task__state state;

            /* Get the highest level */
            pk_task__current_id = pk_bitarray__msbs(&g_task__rq.group);
            /* Execute the task */
            state = g_tasks[pk_task__current_id].fn(
                                                    &g_tasks[pk_task__current_id].context,
                                                    g_tasks[pk_task__current_id].arg);
            switch (state) {
            case PK_TASK__STATE_DORMANT:
            case PK_TASK__STATE_WAITING:
            {
                pk_port__isr_state isr_state = pk_port__isr_disable();
                pk_bitarray__clear(&g_task__rq.group, pk_task__current_id);
                pk_port__isr_restore(isr_state);
                break;
            }
            default:
                break;
            }
        }
        pk_port__sleep();
    }
}
