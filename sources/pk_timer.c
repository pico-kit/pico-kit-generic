
#include "tasker/pk_timer.h"

#include <assert.h>

#include "common/pk_bits.h"
#include "common/pk_port.h"
#include "pico-kit_manifest.h"

struct timer
{
    pk_timer__state state;
    pk_completion__id completion;
    pk__tick current_ticks;
    pk__tick initial_ticks;
    pk_timer__mode mode;
};

static struct timer g_timer_pool[NEON8__MANIFEST__TIMERS];

pk_timer__id
pk_timer__create(pk_completion__id completion, pk__tick ticks, pk_timer__mode mode)
{
    pk_timer__id retval = {.id = PK_TYPES__NULL_INDEX};

    for (pk_types__index new_instance = 0; new_instance < PK_BITS__ARRAY_SIZE(g_timer_pool); new_instance++) {
        if (g_timer_pool[new_instance].state == PK_TIMER__STATE_DORMANT) {
            g_timer_pool[new_instance].state = PK_TIMER__STATE_IDLE;
            g_timer_pool[new_instance].completion = completion;
            g_timer_pool[new_instance].current_ticks = ticks;
            g_timer_pool[new_instance].initial_ticks = ticks;
            g_timer_pool[new_instance].mode = mode;
            retval.id = new_instance;
            break;
        }
    }
    return retval;
}

void
pk_timer__delete(pk_timer__id id);

void
pk_timer__start(pk_timer__id self)
{
    pk_port__isr_state isr_state = pk_port__isr_disable();
    g_timer_pool[self.id].current_ticks = g_timer_pool[self.id].initial_ticks;
    g_timer_pool[self.id].state = PK_TIMER__STATE_COUNTING;
    pk_port__isr_restore(isr_state);
}

void
pk_timer__stop(pk_timer__id self)
{
    pk_port__isr_state isr_state = pk_port__isr_disable();
    g_timer_pool[self.id].state = PK_TIMER__STATE_IDLE;
    pk_port__isr_restore(isr_state);
}

pk_timer__state
pk_timer__get_state(pk_timer__id self)
{
    return g_timer_pool[self.id].state;
}

void
pk_timer__tick_isr(void)
{
    for (pk_types__index i = 0; i < PK_BITS__ARRAY_SIZE(g_timer_pool); i++) {
        g_timer_pool[i].current_ticks--;
        if ((g_timer_pool[i].current_ticks == 0) && (g_timer_pool[i].state == PK_TIMER__STATE_COUNTING)) {
            g_timer_pool[i].current_ticks = g_timer_pool[i].initial_ticks;
            /* Is this ONESHOT timer? */
            if (g_timer_pool[i].mode == PK_TIMER__MODE_ONESHOT) {
                g_timer_pool[i].state = PK_TIMER__STATE_IDLE;
            }
            pk_completion__complete_isr(g_timer_pool[i].completion);
        }
    }
}
