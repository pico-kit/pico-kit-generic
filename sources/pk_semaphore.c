
#include <stddef.h>

#include "common/pk_bits.h"
#include "common/pk_port.h"
#include "composite/pk_bitarray.h"
#include "tasker/pk_semaphore.h"
#include "tasker/pk_task.h"
#include "pico-kit_manifest.h"

enum pk_semaphore__state
{
    PK_SEMAPHORE__STATE_DORMANT,
    PK_SEMAPHORE__STATE_ACTIVE,
};

struct semaphore
{
    uint32_t count;
    struct pk_bitarray wq;
    enum pk_semaphore__state state;
};

#if defined(NEON8__MANIFEST__SEMAPHORES)
static struct semaphore g_semaphore__pool[NEON8__MANIFEST__SEMAPHORES];
#endif

pk_semaphore__id
pk_semaphore__create(uint32_t count)
{
    pk_semaphore__id retval = PK_SEMAPHORE__NULL_ID;
    for (pk_semaphore__id id = 0u; id < PK_BITS__ARRAY_SIZE(g_semaphore__pool); id++) {
        if (g_semaphore__pool[id].state == PK_SEMAPHORE__STATE_DORMANT) {
            g_semaphore__pool[id].state = PK_SEMAPHORE__STATE_ACTIVE;
            g_semaphore__pool[id].count = count;
            pk_bitarray__init(&g_semaphore__pool[id].wq);
            retval = id;
            break;
        }
    }
    return retval;
}

bool
pk_semaphore__is_signaled(pk_semaphore__id id)
{
    bool retval;
    pk_port__isr_state isr_state = pk_port__isr_disable();
    if (g_semaphore__pool[id].count > 0u) {
        g_semaphore__pool[id].count--;
        retval = false;
    } else {
        pk_bitarray__set(&g_semaphore__pool[id].wq, pk_task__current_id);
        retval = true;
    }
    pk_port__isr_restore(isr_state);
    return retval;
}

bool
pk_semaphore__signal(pk_semaphore__id id)
{
    bool retval = false;
    pk_port__isr_state isr_state = pk_port__isr_disable();
    if (g_semaphore__pool[id].count < UINT32_MAX) {
        g_semaphore__pool[id].count++;

        if (g_semaphore__pool[id].count == 1) {
            pk_task__id id;

            id = pk_bitarray__msbs_and_clear(&g_semaphore__pool[id].wq);
            retval = pk_task__unblock(id);
        }
    }
    pk_port__isr_restore(isr_state);
    return retval;
}

enum pk_semaphore__state
pk_semaphore__get_state(pk_semaphore__id id)
{
    return g_semaphore__pool[id].state;
}