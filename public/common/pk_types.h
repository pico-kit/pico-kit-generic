/*
 * Neon
 * Copyright (C) 2021   REAL-TIME CONSULTING
 */
/** @file
 *  @author      Nenad Radulovic
 *  @brief       Common types used in Pico-Kit
 *
 *  @addtogroup  common
 *  @{
 */
/** @defgroup    common_types Types
 *  @brief       Types interface
 *
 *  @{
 */

#ifndef PICO_KIT_COMMON_TYPES_H_
#define	PICO_KIT_COMMON_TYPES_H_

#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

#include <stdint.h>

#define PK_TYPES__NULL_INDEX    UINT_FAST8_MAX

typedef uint_fast8_t pk_types__index;

#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* PICO_KIT_COMMON_TYPES_H_ */

/** @} */