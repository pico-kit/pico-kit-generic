/*
 * Neon
 * Copyright (C) 2018   REAL-TIME CONSULTING
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */
/** @file
 *  @author      Nenad Radulovic
 *  @brief       MCU header
 *
 *  @addtogroup  lib
 *  @{
 */
/** @defgroup    lib_mcu MCU
 *  @brief       MCU
 *  @{
 */
/*---------------------------------------------------------------------------*/


#ifndef PICO_KIT_GENERIC_PORT_MCU_H_
#define PICO_KIT_GENERIC_PORT_MCU_H_

#include "pk_port_variant.h"

#if defined(__DOXYGEN__)
#define PK_MCU__ISR__ENABLE()
#endif

#if defined(__DOXYGEN__)
#define PK_PSP__ISR__DISABLE()
#endif

#if defined(__DOXYGEN__)
#define PK_PSP__ISR__SAVE_DISABLE(state)
#endif

#if defined(__DOXYGEN__)
#define PK_PSP__ISR__RESTORE(state)
#endif

/** @} */
/** @} */
/*---------------------------------------------------------------------------*/
#endif /* NEON_MODULE_NPORT_MCU_H_ */
