/*
 * Neon
 * Copyright (C) 2021   REAL-TIME CONSULTING
 */
/** @file
 *  @author      Nenad Radulovic
 *  @brief       Bitwise manipulations and tricks
 *
 *  @addtogroup  common
 *  @{
 */
/** @defgroup    common_bits Bits
 *  @brief       Bitwise manipulations and tricks
 *
 *  @{
 */

#ifndef PICO_KIT_COMMON_BITS_H_
#define PICO_KIT_COMMON_BITS_H_

#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @defgroup    bits_array Macros to determine size of objects
 * @brief       Macros to determine size of objects
 *
 * @{
 */

/**
 * @brief       Determines the first dimension of an array.
 *
 * @param       array
 *              An array : type any
 * @mseffect
 */
#define PK_BITS__ARRAY_SIZE(array) (sizeof(array) / sizeof((array)[0]))

/**
 * @brief       Returns the size of @a type in bits.
 *
 * @mseffect
 */
#define PK_BITS__BIT_SIZE(type)    (sizeof(type) * 8u)

/**@} */
/**
 * @defgroup    bits_division Integer division
 * @brief       Integer division.
 *
 * @{
 */

/**
 * @brief       Round a division
 *
 * @param       numerator
 * @param       denominator
 * @note        It is desirable that denominator is a constant expression,
 *              otherwise the compiler will generate two division operations.
 * @mseffect
 * @par         Example 1:
 *
 *              numerator   = 28
 *              denominator = 5
 *
 *              Integer division: 28 / 5 = 5
 *              Float division  : 28 / 5 = 5.6
 *              Rounded division: 28 / 5 = 6
 *
 * @par         Example 2:
 *
 *              numerator   = 27
 *              denominator = 5
 *
 *              Integer division: 27 / 5 = 5
 *              Float division  : 27 / 5 = 5.4
 *              Rounded division: 27 / 5 = 5
 */
#define PK_BITS__DIVIDE_ROUND(numerator, denominator)                       \
    (((numerator) + ((denominator) / 2u)) / (denominator))

/** @brief      Round up a division
 *  @param      numerator
 *  @param      denominator
 *  @note       It is desirable that denominator is a constant expression,
 *              otherwise the compiler will generate one subtraction and one
 *              division operation.
 *  @mseffect
 *  @par        Example 1:
 *
 *              numerator   = 28
 *              denominator = 5
 *
 *              Integer division   : 28 / 5 = 5
 *              Float division     : 28 / 5 = 5.6
 *              Rounded up division: 28 / 5 = 6
 *
 *  @par        Example 2:
 *
 *              numerator   = 27
 *              denominator = 5
 *
 *              Integer division   : 27 / 5 = 5
 *              Float division     : 27 / 5 = 5.4
 *              Rounded up division: 27 / 5 = 6
 */
#define PK_BITS__DIVIDE_ROUNDUP(numerator, denominator)                     \
    (((numerator) + (denominator) - 1u) / (denominator))

/**@} */
/**
 * @defgroup    bits_align Data alignment
 * @brief       Data alignment.
 *
 * @{
 */

/**
 * @brief       Do the alignment of @a num value as specified by @a align.
 *
 * @param       num
 *              A value which needs to be aligned.
 * @param       align
 *              Align value.
 * @mseffect
 * @par         Example 1:
 *
 *              num  : 10010101 = 149
 *              align: 00000100 = 4
 *              Result is 148.
 */
#define PK_BITS__ALIGN(num, align) ((num) & ~((align) - 1u))

/**
 * @brief       Do the alignment of @a num value as specified by @a align.
 *
 * In contrast to @ref NBITS_ALIGN, this macro will always pick a next bigger
 * value for alignment.
 *
 * @param       num
 *              A value which needs to be aligned.
 * @param       align
 *              Align value.
 * @par         Example 1:
 *
 *              num  : 10010101 = 149
 *              align: 00000100 = 4
 *              Result is 152.
 */
#define PK_BITS__ALIGN_UP(num, align)                                       \
    (((num) + (align) - 1u) & ~((align) - 1u))

/**@} */
/**
 * @defgroup   bits_log Logarithm calculation
 * @brief      Logarithm calculation.
 *
 * @{
 */

/** @brief      Calculate log2 for value @c x during the compilation.
 *
 *  This macro is best used when @c x is a constant. If the argument is a
 *  variable there are faster approaches.
 *
 *  @note       The @c x argument has to be in 0 - 255 range.
 *  @mseffect
 */
#define PK_BITS__LOG2_8(x)                                                     \
    ((x) <   2u ? 0u :                                                      \
     ((x) <   4u ? 1u :                                                     \
      ((x) <   8u ? 2u :                                                    \
       ((x) <  16u ? 3u :                                                   \
        ((x) <  32u ? 4u :                                                  \
         ((x) <  64u ? 5u :                                                 \
          ((x) < 128u ? 6u : 7u)))))))

/** @} */
/*---------------------------------------------------------------------------*/
/** @defgroup   bits_power2 Power of 2 macros
 *  @brief      Power of 2 macros.
 *  @{
 */

#define PK_BITS__IS_POWEROF2(num)    										    \
    (((num) != 0u) && (((num) & ((num) - 1)) == 0u))

/**@} */
/*--------------------------------------------------------------------------*/
/**@defgroup    bits_byteextract Byte extract
 * @brief       Byte extract.
 * @{
 */

/**@brief       Sign extend signed 24 bit integer to signed 32 bits integer
 * @param       val - signed 24 bit integer
 * @return
 */
int32_t pk_bits__sext_i24(int32_t value);

/**@} */
/*---------------------------------------------------------------------------*/
/**@defgroup    bits_conv Data convert
 * @brief       Data convert.
 * @{
 */

uint32_t pk_bits__ftou32(float val);

float pk_bits__u32tof(uint32_t val);

/**@} */
#ifdef __cplusplus
}
#endif

/**@} */
/**@} */
#endif /* PICO_KIT_COMMON_BITS_H_ */

