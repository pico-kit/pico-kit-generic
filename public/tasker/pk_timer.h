
#ifndef PICO_KIT_TIMER_H_
#define	PICO_KIT_TIMER_H_

#include <stdint.h>

#include "common/pk_types.h"
#include "tasker/pk_completion.h"

typedef struct pk_timer__id_struct {
    pk_types__index id;
} pk_timer__id;

typedef uint16_t pk__tick;

typedef enum pk_timer__mode {
    PK_TIMER__MODE_ONESHOT,
    PK_TIMER__MODE_PERIODIC
} pk_timer__mode;

typedef enum pk_timer__state {
    PK_TIMER__STATE_DORMANT,
    PK_TIMER__STATE_IDLE,
    PK_TIMER__STATE_COUNTING,
    PK_TIMER__STATE_DELETED
} pk_timer__state;

pk_timer__id
pk_timer__create(pk_completion__id completion, pk__tick ticks, pk_timer__mode mode);

void
pk_timer__delete(pk_timer__id id);

void
pk_timer__start(pk_timer__id id);

void
pk_timer__stop(pk_timer__id id);

pk_timer__state
pk_timer__get_state(pk_timer__id id);

void
pk_timer__tick_isr(void);

#endif	/* PICO_KIT_TIMER_H_ */

