
#ifndef PICO_KIT_GENERIC_TASKER_COMPLETION_H_
#define PICO_KIT_GENERIC_TASKER_COMPLETION_H_

#include <stdint.h>
#include <stdbool.h>

#include "tasker/pk_task.h"

#define PK_COMPLETION__NULL_ID     UINT8_MAX

typedef uint8_t pk_completion__id;

pk_completion__id pk_completion__create(pk_task__id id);

void
pk_completion__complete_isr(pk_completion__id id);

bool pk_completion__complete(pk_completion__id id);

bool pk_completion__is_completed(pk_completion__id id);

void pk_completion__reset(pk_completion__id id);

#endif /* PICO_KIT_GENERIC_TASKER_COMPLETION_H_ */