/**
 * @file
 * @brief       Pico-Kit Tasker interface header
 *
 * @author      (NR) Nenad Radulovic (nenad.b.radulovic@gmail.com)
 *
 * @li          2022-08-01 - NR: Initial release
 *
 * @defgroup    tasker Pico-Kit Tasker interface
 * @brief       Tasker interface
 *
 * @{
 */

#ifndef PICO_KIT_TASKER_H_
#define PICO_KIT_TASKER_H_

#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief       Declare the start of a task inside the C function.
 *
 * This macro is used to declare the starting point of a task. It should be
 * placed at the beginning of the function in which the task runs. All C
 * statements above the PK_TASK__BEGIN() invocation will be executed each time
 * the task is scheduled.
 *
 * @param       ctx
 *              A pointer to the task context structure, @ref pk_task__context.
 * @hideinitializer
 */
#define PK_TASK__BEGIN(ctx)                                                 \
    do {                                                                    \
        switch(*(ctx)) {                                                    \
        case 0:

/**
 * @brief       Declare the end of a task.
 *
 * This macro is used for declaring that a fiber ends. It must always be used
 * together with a matching PK_TASK__BEGIN() macro.
 *
 * @param       ctx
 *              A pointer to the task context structure, @ref pk_task__context.
 * @hideinitializer
 */
#define PK_TASK__END(ctx)                                                   \
            *(ctx) = __LINE__; case __LINE__:                               \
            default:                                                        \
                return PK_TASK__STATE_DORMANT;                              \
        }                                                                   \
    } while (0)

/**
 * @brief       Exit the task.
 *
 * This macro causes the task to exit. If the task was spawned by another
 * task, the parent task will become unblocked and can continue to run.
 *
 * @param       ctx
 *              A pointer to the task context structure, @ref pk_task__context.
 * @hideinitializer
 */
#define PK_TASK__EXIT(ctx)                                                  \
    *(ctx) = __LINE__; case __LINE__: ; return PK_TASK__STATE_DORMANT

/**
 * @brief       Block and wait until condition is true.
 *
 * This macro blocks the fiber until the specified action is set to continue.
 *
 * @param       ctx
 *              A pointer to the task context structure, @ref pk_task__context.
 * @param       should_wait
 *              If this boolean argument is set to `true` it will block the task.
 * @hideinitializer
 */
#define PK_TASK__WAIT(ctx, should_wait)                                     \
    if (should_wait) {                                                      \
        *(ctx) = __LINE__; return PK_TASK__STATE_WAITING; case __LINE__: ;  \
    }

/**
 * @brief       Yield from the current fiber.
 *
 * This function will yield the task, thereby allowing other processing to take
 * place in the system.
 *
 * @param       ctx
 *              A pointer to the task context structure, @ref pk_task__context.
 * @hideinitializer
 */
#define PK_TASK__YIELD(ctx)                                                 \
    *(ctx) = __LINE__; return PK_TASK__STATE_RUNNING; case __LINE__:

#define PK_TASK__SPAWN(ctx, callable)                                        \
    do {                                                                    \
        pk_task__state state = (callable);                                  \
        if (state == PK_TASK__STATE_DORMANT) {                           \
            break;                                                          \
        }                                                                   \
        *(ctx) = __LINE__; return state; case __LINE__:                     \
    } while (true)

#define PK_TASK__NULL_ID        UINT8_MAX

typedef enum pk_task__state {
    PK_TASK__STATE_DORMANT,
    PK_TASK__STATE_RUNNING,
    PK_TASK__STATE_WAITING
} pk_task__state;

typedef uint_fast16_t pk_task__context;

typedef uint8_t pk_task__id;

typedef pk_task__state(pk_task__fn)(pk_task__context *, void *);

extern pk_task__id pk_task__current_id;

pk_task__id
pk_task__create(uint8_t priority, pk_task__fn * fn, void * arg);

bool
pk_task__unblock(pk_task__id id);

void
pk_task__schedule(void);


#ifdef __cplusplus
}
#endif

/** @} */
#endif /* PICO_KIT_TASKER_H_ */
