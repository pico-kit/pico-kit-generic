
#ifndef PICO_KIT_SEMAPHORE_H_
#define PICO_KIT_SEMAPHORE_H_

#include <stdint.h>

#define PK_SEMAPHORE__NULL_ID        UINT8_MAX

typedef uint8_t pk_semaphore__id;

pk_semaphore__id 
pk_semaphore__create(uint32_t count);

/**
 * Wait for a semaphore
 *
 * This macro carries out the "wait" operation on the semaphore. The
 * wait operation causes the protothread to block while the counter is
 * zero. When the counter reaches a value larger than zero, the
 * protothread will continue.
 *
 * \param s (struct nsem *) A pointer to the nsem struct
 * representing the semaphore
 *
 * \hideinitializer
 */
bool pk_semaphore__is_signaled(pk_semaphore__id id);

/**
 * Signal a semaphore
 *
 * This macro carries out the "signal" operation on the semaphore. The
 * signal operation increments the counter inside the semaphore, which
 * eventually will cause waiting protothreads to continue executing.
 *
 * \param pt (struct pt *) A pointer to the protothread (struct pt) in
 * which the operation is executed.
 *
 * \param s (struct nsem *) A pointer to the nsem struct
 * representing the semaphore
 *
 * \hideinitializer
 */
bool pk_semaphore__signal(pk_semaphore__id id);

#endif /* PICO_KIT_SEMAPHORE_H_ */
/** @} */

