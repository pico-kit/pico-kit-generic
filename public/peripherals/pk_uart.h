
#ifndef PICO_KIT_GENERIC_PERIPHERALS_UART_H_
#define	PICO_KIT_GENERIC_PERIPHERALS_UART_H_

#include "pico-kit-generic/public/common/pk_configuration.h"

#if (PK_CONFIGURATION__UART__IS_ENABLED == 1)

#include <stddef.h>

#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

typedef enum pk_uart__id {
#if (PK_CONFIGURATION__UART__0__IS_ENABLED == 1)
    PK_UART__0,
#endif
#if (PK_CONFIGURATION__UART__1__IS_ENABLED == 1)
    PK_UART__1,
#endif
#if (PK_CONFIGURATION__UART__2__IS_ENABLED == 1)
    PK_UART__2,
#endif
#if (PK_CONFIGURATION__UART__3__IS_ENABLED == 1)
    PK_UART__3,
#endif
} pk_uart__id;

#define PK_UART__FLAGS__DATA_8BIT       (0x1 << 0)
#define PK_UART__FLAGS__DATA_9BIT       (0x2 << 0)
#define PK_UART__FLAGS__DATA_7BIT       (0x3 << 0)

#define PK_UART__FLAGS__PARITY_NONE     (0x1 << 2)
#define PK_UART__FLAGS__PARITY_ODD      (0x2 << 2)
#define PK_UART__FLAGS__PARITY_EVEN     (0x3 << 2)

#define PK_UART__FLAGS__STOP_1BIT       (0x1 << 4)
#define PK_UART__FLAGS__STOP_2BIT       (0x2 << 4)

#define PK_UART__FLAGS__ENABLE_RX       (0x1 << 6)
#define PK_UART__FLAGS__ENABLE_TX       (0x2 << 6)

void
pk_uart__init_all(void);

void
pk_uart__setup(pk_uart__id id, uint8_t flags, uint32_t baudrate);

void pk_uart__write(pk_uart__id id, const void * data, size_t size);
void pk_uart__read(pk_uart__id id, void * data, size_t size);

#if (PK_CONFIGURATION__UART__DYNAMIC_CALLBACK == 1)
void pk_uart__async__write(pk_uart__id id, const void * data, size_t size);
void pk_uart__async__write_abort(pk_uart__id id);
void pk_uart__async__read(pk_uart__id id, void * data, size_t size);
void pk_uart__async__read_abort(pk_uart__id id);
void pk_uart__async__write_callback(pk_uart__id id, void (*callback)(void));
void pk_uart__async__read_callback(pk_uart__id id, void (*callback)(void));
#else /* (PK_CONFIGURATION__UART__DYNAMIC_CALLBACK == 1) */
extern uint8_t pk_uart__0__async__write__buffer[];
extern uint8_t pk_uart__1__async__write__buffer[];
extern uint8_t pk_uart__2__async__write__buffer[];
extern uint8_t pk_uart__3__async__write__buffer[];

extern uint8_t pk_uart__0__async__read__buffer[];
extern uint8_t pk_uart__1__async__read__buffer[];
extern uint8_t pk_uart__2__async__read__buffer[];
extern uint8_t pk_uart__3__async__read__buffer[];

extern void pk_uart__0__async__write_complete(void);
extern void pk_uart__1__async__write_complete(void);
extern void pk_uart__2__async__write_complete(void);
extern void pk_uart__3__async__write_complete(void);

extern void pk_uart__0__async__read_complete(void);
extern void pk_uart__1__async__read_complete(void);
extern void pk_uart__2__async__read_complete(void);
extern void pk_uart__3__async__read_complete(void);

void pk_uart__async__write(pk_uart__id id, size_t size);
void pk_uart__async__write_abort(pk_uart__id id);
void pk_uart__async__read(pk_uart__id id, size_t size);
void pk_uart__async__read_abort(pk_uart__id id);

uint8_t * pk_uart__async__write_buffer(pk_uart__id id);
const uint8_t * pk_uart__async__read_buffer(pk_uart__id id);

#endif /* !(PK_CONFIGURATION__UART__DYNAMIC_CALLBACK == 1) */
#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif /* (PK_CONFIGURATION__UART__IS_ENABLED == 1) */
#endif	/* PICO_KIT_GENERIC_PERIPHERALS_UART_H_ */

