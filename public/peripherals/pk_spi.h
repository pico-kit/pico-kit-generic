
#ifndef PICO_KIT_GENERIC_PERIPHERALS_SPI_H_
#define	PICO_KIT_GENERIC_PERIPHERALS_SPI_H_

#include "pico-kit-generic/public/common/pk_configuration.h"

#if (PK_CONFIGURATION__SPI__IS_ENABLED == 1)

#include <stdint.h>
#include <stddef.h>

#include "pico-kit-generic/public/peripherals/pk_gpio.h"

#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

typedef enum pk_spi__id {
#if (PK_CONFIGURATION__SPI__0__IS_ENABLED == 1)
    PK_SPI__0,
#endif
#if (PK_CONFIGURATION__SPI__1__IS_ENABLED == 1)
    PK_SPI__1,
#endif
} pk_spi__id;

#define PK_SPI__FLAGS__ENABLE_SDO       0x1u << 0
#define PK_SPI__FLAGS__ENABLE_SDI       0x1u << 1
#define PK_SPI__FLAGS__CLOCK_RISING     0x1u << 2
#define PK_SPI__FLAGS__CLOCK_FALLING    0x1u << 3
#define PK_SPI__FLAGS__CLOCK_IDLE_HIGH  0x1u << 4
#define PK_SPI__FLAGS__CLOCK_IDLE_LOW   0x1u << 5
#define PK_SPI__FLAGS__SS_HIGH          0x1u << 6
#define PK_SPI__FLAGS__SS_LOW           0x1u << 7

extern uint8_t pk_spi__g__0__buffer[PK_CONFIGURATION__SPI_0_BUFFER];

void
pk_spi__init_all(void);

void
pk_spi__setup(pk_spi__id id,
              uint8_t configuration,
              uint32_t clock,
              pk_gpio__id gpio_cs);

void
pk_spi__power__disable(pk_spi__id id);

void
pk_spi__power__enable(pk_spi__id id);

void
pk_spi__enable(pk_spi__id id);

void
pk_spi__disable(pk_spi__id id);

void
pk_spi__write(pk_spi__id id,
              size_t size);

void pk_spi__0__exchg(size_t size);
void pk_spi__0__select(void);
void pk_spi__0__write_selected(size_t size);
void pk_spi__0__read_selected(size_t size);
void pk_spi__0__unselect(void);


#ifdef	__cplusplus
}
#endif /* __cplusplus */
#endif /* (PK_CONFIGURATION__SPI__IS_ENABLED == 1) */
#endif	/* PICO_KIT_GENERIC_PERIPHERALS_SPI_H_ */

