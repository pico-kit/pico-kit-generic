
#ifndef PICO_KIT_GENERIC_PERIPHERALS_GPIO_H_
#define	PICO_KIT_GENERIC_PERIPHERALS_GPIO_H_

#include "pico-kit-psp/variant/gpio.h"

#ifdef	__cplusplus
extern "C"
{
#endif /* __cplusplus */

#define PK_GPIO__ID_FROM(port, pin)                                         \
        (((port) << 5u) | (pin))
    
#define PK_GPIO__PORT_FROM(id)                                              \
        ((id) >> 5u)
    
#define PK_GPIO__PIN_FROM(id)                                               \
        ((id) & 0x1fu)

typedef uint8_t pk_gpio__id;

void pk_gpio__init_input(pk_gpio__id id);
void pk_gpio__setup_output(pk_gpio__id id);
void pk_gpio__set(pk_gpio__id id);

#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* PICO_KIT_GENERIC_PERIPHERALS_GPIO_H_ */

