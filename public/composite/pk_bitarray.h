/**
 * @file
 * @brief       Pico-Kit Bit-array interface header
 *
 * @author      (NR) Nenad Radulovic (nenad.b.radulovic@gmail.com)
 *
 * @li          2022-08-01 - NR: Initial release
 *
 * @defgroup    bitarray Pico-Kit Bit-array interface
 * @brief       Bit-array interface
 *
 * @{
 */

#ifndef PICO_KIT_BITARRAY_H_
#define PICO_KIT_BITARRAY_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#define PK_BITARRAY__MAX_BITS           64u

/**
 * @brief       Bitarray structure
 *
 * Each bitarray contains actual bits which are stored in `rows` member and
 * `column` variable which notes which `rows` contains at least one one '1'.
 *
 * @api
 */
struct pk_bitarray {
    uint8_t column;
    uint8_t rows[8];
};

/**
 * @brief      Initialize the bitarray.
 *
 * All bits in the pointed bitarray will be set to zero '0'.
 *
 * @param      ba
 *             Pointer to bitarray structure @ref pk_bitarray.
 * @api
 */
void pk_bitarray__init(struct pk_bitarray * ba);

/**
 * @brief       Set a bit in the array.
 *
 * @param       ba
 *              Pointer to bitarray structure @ref pk_bitarray.
 * @param       bit_index
 *              Bit index in the bitarray. The bit value must be in range:
 *              0 - (@ref PK_BITARRAY__MAX_BITS - 1u).
 * @note        Using a value bigger than @ref PK_BITARRAY__MAX_BITS - 1u for
 *              argument bit_index will cause undefined behavior.
 * @api
 */
void pk_bitarray__set(struct pk_bitarray * ba, uint_fast8_t bit_index);

/**
 * @brief       Clear a bit in the array.
 *
 * @param       ba
 *              Pointer to bitarray structure @ref pk_bitarray.
 * @param       bit_index
 *              Bit index in the bitarray. The bit value must be in range:
 *              0 - (@ref PK_BITARRAY__MAX_BITS - 1u). Type uint_fast8_t.
 * @note        Using a value bigger than @ref PK_BITARRAY__MAX_BITS - 1u for
 *              argument bit_index will cause undefined behavior.
 * @api
 */
void pk_bitarray__clear(struct pk_bitarray * ba, uint_fast8_t bit_index);

/**
 * @brief       Get the index of Most Significant Bit Set.
 *
 * @param       ba
 *              Pointer to bitarray structure @ref pk_bitarray.
 * @returns     The position of the MSB set bit. The range of returned value is
 *              0 - @ref PK_BITARRAY__MAX_BITS - 1u.
 * @note        If no bit is set in the array, the result of this function is
 *              undefined; ensure that a bit is set by using
 *              @ref pk_bitarray__is_empty
 * @api
 */
uint_fast8_t pk_bitarray__msbs(const struct pk_bitarray * ba);

/**
 * @brief       Get the index of Most Significant Bit Set and then clear it.
 *
 * @param       ba
 *              Pointer to bitarray structure @ref pk_bitarray.
 * @returns     The position of the MSB set bit. The range of returned value is
 *              0 - @ref PK_BITARRAY__MAX_BITS - 1u.
 * @note        If no bit is set in the array, the result of this function is
 *              undefined; ensure that a bit is set by using
 *              @ref pk_bitarray__is_empty
 * @api
 */
uint_fast8_t pk_bitarray__msbs_and_clear(struct pk_bitarray * ba);

/**
 * @brief       Returns if bit-array is empty.
 *
 * @param       ba
 *              Pointer to bitarray structure @ref pk_bitarray.
 * @api
 */
#define pk_bitarray__is_empty(a_ba)     ((a_ba)->column == 0u)

#ifdef __cplusplus
}
#endif

/**@} */
#endif /* PICO_KIT_BITARRAY_H_ */
