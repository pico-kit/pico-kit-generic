
#ifndef VA_INCLUDE_NPORT_VARIANT_MCU_H_
#define	VA_INCLUDE_NPORT_VARIANT_MCU_H_

#include <xc.h>

typedef uint8_t pk_port__isr_state;

static inline void
pk_port__isr_enable(void)
{
    INTCONbits.GIEH = 1;
}

static inline pk_port__isr_state
pk_port__isr_disable(void)
{
    uint8_t retval = INTCON;
    INTCONbits.GIEH = 0;
    return retval;
}

static inline void
pk_port__isr_restore(pk_port__isr_state isr_state)
{
    INTCON |= isr_state & _INTCON_GIEH_MASK;
}

static inline void
pk_port__sleep(void)
{

}

void pk_port__isr_init(void);

    
#define pk_port__cpu_stop()           for (;;)
    
#define pk_port__cpu_wait()           SLEEP()

#define pk_port__set_bit(u8_data, u8_bit)                                     \
    switch (u8_bit) {                                                       \
        case 0:                                                             \
            *u8_data |= 0x1 << 0;                                            \
            break;                                                          \
        case 1:                                                             \
            *u8_data |= 0x1 << 1;                                            \
            break;                                                          \
        case 2:                                                             \
            *u8_data |= 0x1 << 2;                                            \
            break;                                                          \
        case 3:                                                             \
            *u8_data |= 0x1 << 3;                                            \
            break;                                                          \
        case 4:                                                             \
            *u8_data |= 0x1 << 4;                                            \
            break;                                                          \
        case 5:                                                             \
            *u8_data |= 0x1 << 5;                                            \
            break;                                                          \
        case 6:                                                             \
            *u8_data |= 0x1 << 6;                                            \
            break;                                                          \
        case 7:                                                             \
            *u8_data |= 0x1 << 7;                                            \
            break;                                                          \
    }
    
#define pk_port__clear_bit(u8_data, u8_bit)                                   \
    do {                                                                    \
        *(u8_data) &= narch__inv_exp2(u8_bit);                              \
    } while (0)
    
#define narch__exp2(u8_x)           ng_exp2_table[u8_x]
        
#define narch__inv_exp2(u8_x)       ng_inv_exp2_table[u8_x]
    
#define narch__log2(u8_x)           ng_log2_table[u8_x]
    
extern const uint_fast8_t ng_exp2_table[8];

extern const uint_fast8_t ng_inv_exp2_table[8];

extern const uint_fast8_t ng_log2_table[256];


#endif	/* VA_INCLUDE_NPORT_VARIANT_MCU_H_ */
