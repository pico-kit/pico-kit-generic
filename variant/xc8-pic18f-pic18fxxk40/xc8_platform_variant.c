/*
 * Neon
 * Copyright (C) 2018   REAL-TIME CONSULTING
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "common/pk_port.h"


void
pk_port__isr_init(void)
{
    INTCONbits.IPEN = 1;
}

const uint_fast8_t ng_exp2_table[8] =
{
    (0x1u << 0u),
    (0x1u << 1u),
    (0x1u << 2u),
    (0x1u << 3u),
    (0x1u << 4u),
    (0x1u << 5u),
    (0x1u << 6u),
    (0x1u << 7u),
};

const uint_fast8_t ng_inv_exp2_table[8] =
{
    (uint8_t)~(0x1u << 0u),
    (uint8_t)~(0x1u << 1u),
    (uint8_t)~(0x1u << 2u),
    (uint8_t)~(0x1u << 3u),
    (uint8_t)~(0x1u << 4u),
    (uint8_t)~(0x1u << 5u),
    (uint8_t)~(0x1u << 6u),
    (uint8_t)~(0x1u << 7u),
};

const uint_fast8_t ng_log2_table[256] =
{
#define LT(n) n, n, n, n, n, n, n, n, n, n, n, n, n, n, n, n
    0, 0, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3,
    LT(4), LT(5), LT(5), LT(6), LT(6), LT(6), LT(6),
    LT(7), LT(7), LT(7), LT(7), LT(7), LT(7), LT(7), LT(7)
#undef LT
};